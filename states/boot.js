var boot = function(game){
	console.log("%cStarting boot state", "color:white; background:red");
};
  
boot.prototype = {
	preload: function(){
	    
	},
    create: function(){
    	if(!this.game.device.desktop) {
			this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
			this.scale.minWidth = 320;
			this.scale.minHeight = 480;
			this.scale.maxWidth = 640;
			this.scale.maxHeight = 1136;
			this.game.scale.refresh();
			
		}
		this.scale.pageAlignHorizontally = true;
		this.scale.setScreenSize();
		this.game.state.start("Preload");
	}
};