var gameover = function(game){
	console.log("%cStarting gameover state", "color:white; background:red");
	var offset;
	var scale;
	var playerScore;
};
 
gameover.prototype = {
	init: function(score){
		playerScore = score;
	},
   	create: function(){
   		var bg = this.game.add.sprite(0, 0, 'background');

		if(this.game.device.desktop) {
			offset = 0.1;
			scale = 1;
			bg.scale.setTo(800, 0.52817);
		} else {
			offset = 0.3;
			scale = 1;
			bg.scale.setTo(640, 1);
		}

		var logoBtn = this.game.add.button(this.game.width / 2 - this.game.cache.getImage('logoNoText').width / 2, this.game.height*offset, 'logoNoText', this.clicked);
		var scoreText = this.game.add.text(0, 0, "Score " + playerScore, { font: "35px Arial", fill: "#fff", align: "center" });
		scoreText.wordWrap = true;
		scoreText.wordWrap = 50;
		scoreText.x = (this.game.width / 2) - (scoreText.width / 2);
		scoreText.y = (this.game.world.centerY) - (this.game.world.centerY / 2.75);
		
		var gameOverText = this.game.add.text(0, 0, "GAME OVER!", { font: "30px Arial", fill: "#000", align: "center" });
		gameOverText.x = (this.game.width / 2) - (gameOverText.width / 2);
		gameOverText.y = (this.game.world.centerY) + (this.game.world.centerY / 2);

		var playAgainText = this.game.add.text(0, 0, "Press the logo to play again!", { font: "20px Arial", fill: "#000", align: "center" });
		playAgainText.x = (this.game.width / 2) - (playAgainText.width / 2);
		playAgainText.y = (this.game.world.centerY) + (this.game.world.centerY / 2) + gameOverText.height;

		var iconY = gameOverText.y - this.game.cache.getImage('websiteIcon').height / 4;
    	var websiteBtn = this.game.add.button(0, iconY, 'websiteIcon', this.clicked, this, 1, 0);
    	var facebookBtn = this.game.add.button(this.game.width - this.game.cache.getImage('facebookIcon').width/2, iconY, 'facebookIcon', this.clicked, this, 1, 0);
 	},
	clicked: function(){
		var pressedButton = arguments[0].key;
		var url;
		switch(pressedButton){
			case "websiteIcon":
				window.open('http://polyjumpergame.com');
				break;
			case "facebookIcon":
				window.open('http://facebook.com/polyjumpergame');
				break;
			case "logoNoText":
				this.game.state.start("Play");
				break;
		}
	}
};