var menu = function(game){
	console.log("%cStarting menu state", "color:white; background:red");
	var offset;
	var scale;
};
 
menu.prototype = {
	create: function(){
		var back = this.game.add.sprite(0, 0, 'background');
		if(this.game.device.desktop) {
			offset = 0.05;
			scale = 1;
			back.scale.setTo(800, 0.52817);
		} else {
			offset = 0.1;
			scale = 1.2;
			back.scale.setTo(640, 1);
		}
        
    	var logo = this.game.add.sprite(this.game.width / 2 - this.game.cache.getImage('logo').width / 2, this.game.height*offset, 'logo');
    	var btn = this.game.add.button(this.game.width / 2 - this.game.cache.getImage('buttonstart').width / 4, (this.game.world.centerY + this.game.cache.getImage('logo').height / 2.5), 'buttonstart', this.clicked, this, 1, 0);
 	},
	clicked: function(){
		this.game.state.start("Play");
	}
};