var preload = function(game){
	console.log("%cStarting preload state", "color:white; background:red");
};
 
preload.prototype = {
	preload: function(){
		this.game.load.image('background', 'assets/background.png');
		this.game.load.spritesheet('button', 'assets/menu/btn.png', 132, 100);
	    this.game.load.spritesheet('buttonstart', 'assets/menu/btnstart.png', 132, 100);
	    this.game.load.image('logo', 'assets/menu/logo.png');

	    this.game.load.image('shardring', 'assets/game/shardring.png');
	    this.game.load.image('cloud_s', 'assets/game/cloud2.png');
	    this.game.load.image('cloud_l', 'assets/game/cloud.png');
	    this.game.load.image('platform_high', 'assets/game/platform3.png');
	    this.game.load.image('platform_mid', 'assets/game/platform2.png');
	    this.game.load.image('platform_low', 'assets/game/platform.png');
	    this.game.load.image('misc_high', 'assets/game/misc/platform3.png');
	    this.game.load.image('misc_mid', 'assets/game/misc/platform2.png');
	    this.game.load.image('misc_low', 'assets/game/misc/platform.png');
	    this.game.load.image('bar', 'assets/game/bar.png');
	    this.game.load.spritesheet('crystal', 'assets/game/crystalspritesheet.png', 23, 38);
	    //this.game.load.spritesheet('character', 'assets/game/character.png', 32, 48);
	    this.game.load.spritesheet('character', 'assets/game/Thanos.png', 50, 61);

	    this.game.load.image('logoNoText', 'assets/gameover/logoNoText.png');
	    this.game.load.spritesheet('websiteIcon', 'assets/gameover/website.png', 120, 138);
	    this.game.load.spritesheet('facebookIcon', 'assets/gameover/facebook.png', 120, 138);

        this.game.load.audio('sfx', 'assets/game/testSounds.ogg');
        this.game.load.audio('ingamesound', 'assets/game/ingameSound.ogg');

	},
   create: function(){
		this.game.state.start("Menu");
	}
};