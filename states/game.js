var play = function(game){
	console.log("%cStarting game state", "color:white; background:red");
	console.log("%cMmmmmmmmh, van delmora", "color:white; background:white");
	var player;
	var platforms;
	var border;
	var clouds;
	var crystals;
	var cursors;
	var score;
	var scoreText;
	var scale;
	var offset;
	var jumpBoost;
    var soundSprite;
    var ingameSound;
};
 
play.prototype = {
	create: function(){
		var bg = this.game.add.sprite(0, 0, 'background');

		this.game.physics.startSystem(Phaser.Physics.arcade);
		if(this.game.device.desktop) {
			scale = 1.25;
			bg.scale.setTo(2400, 0.52817);
			this.game.world.setBounds(0, 0, 2400, 600);
			offset = 1;
			jumpBoost = 1.4;
		} else {
			scale = 1.5;
			bg.scale.setTo(2400, 1);
			this.game.world.setBounds(0, 0, 2400, 1136);
			var bar = this.game.add.sprite(0, this.game.height - 75, 'bar');
			bar.fixedToCamera = true;
			offset = 1.775;
			jumpBoost = 1.775;
		}
	    
	    this.game.add.sprite(0, 0, 'shardring');
	    
	    //Cloud spawning
	    clouds = this.game.add.group();
	    var smallClouds = {
	    	'X':[40, 238, 422, 582, 744, 924, 1078, 1194, 1303, 1423, 1602, 1748, 1940, 2052, 2185, 2342], 
	    	'Y':[20,  45,  25,  53,  34,  14,   32,   18,   48,   33,   48,   18,   23,   34,   28,   20]
	    };
	    var bigClouds = {
	    	'X':[-75, 310, 800, 1750, 2200],
	    	'Y':[95, 80, 101, 94, 89]
	    };
	    for(var i = 0; i < 16; i++) {
	    	var smallcloud = clouds.create(smallClouds.X[i], smallClouds.Y[i]*offset, 'cloud_s');
	    }
	    for(var i = 0; i < 5; i++) {
	    	var bigcloud = clouds.create(bigClouds.X[i], bigClouds.Y[i]*offset, 'cloud_l');
	    }

	    platforms = this.game.add.group();
	    platforms.enableBody = true;

	    //High platforms (snow)
	    var highPlatformLocations = {'X':[20, 520, 805, 1385, 1790], 'Y':[120, 175, 120, 120, 150]};
	    for(var i = 0; i < 5; i++) {
	    	var high = platforms.create(highPlatformLocations.X[i], highPlatformLocations.Y[i]*offset, 'platform_high');
	    	high.body.immovable = true;
	    	//var highmisc = this.game.add.sprite(highPlatformLocations.X[i]*offset, (highPlatformLocations.Y[i]-(19*scale)), 'misc_high');
	    	high.scale.setTo(scale);
	    	//highmisc.scale.setTo(scale);
	    }

	    //Middle platforms (tree and waterfall)
	    var midPlatformLocations = {'X':[175, 695, 1100, 1590, 2150], 'Y':[260, 325, 280, 310, 260]};
	    for(var i = 0; i < 5; i++) {
	    	var mid = platforms.create(midPlatformLocations.X[i], midPlatformLocations.Y[i]*offset, 'platform_mid');
	    	mid.body.immovable = true;
	    	//var midmisc = this.game.add.sprite(midPlatformLocations.X[i]+(8*scale), midPlatformLocations.Y[i]-(26*scale), 'misc_mid');
	    	mid.scale.setTo(scale);
	    	//midmisc.scale.setTo(scale);
	    }

	    //Low platforms (forest)
	    var lowPlatformLocations = {'X':[300, 1300, 1950], 'Y':[420, 400, 415]};
	    for(var i = 0; i < 3; i++) {
	    	var low = platforms.create(lowPlatformLocations.X[i], lowPlatformLocations.Y[i]*offset, 'platform_low');
	    	low.body.immovable = true;
	    	//var lowmisc = this.game.add.sprite(lowPlatformLocations.X[i], lowPlatformLocations.Y[i]-(21*scale), 'misc_low');
	    	low.scale.setTo(scale);
	    	//lowmisc.scale.setTo(scale);
	    }

	    //Moving platform
	    // var moving_low = platforms.create(600, 400, 'platform_low');
	    // moving_low.body.immovable = true;
	    // this.game.add.tween(moving_low).to({x: 500}, 3000).yoyo(true).repeat(-1).start();

	    //Crystal spawning
	    crystals = this.game.add.group();
	    crystals.enableBody = true;
	    var crystalLocation = {'X' : [200, 447, 708, 834, 910, 1104, 1495, 1620, 1800, 1965, 2175, 2325], 'Y' : [100, 260, 120, 247, 40, 200, 60, 200, 95, 350, 200, 100]};
	    for(var i = 0; i < 12; i++) {
	    	var crystal = crystals.create(crystalLocation.X[i], crystalLocation.Y[i]*offset, 'crystal');
		    crystal.animations.add('glow');
		    crystal.animations.play('glow', 9, true);
		    crystal.scale.setTo(scale);
	    }

	    //Player spawning
	    player = this.game.add.sprite(35, 20*offset, 'character');
	    player.scale.setTo(scale);
	    this.game.physics.arcade.enable(player);
	    player.body.gravity.y = 600;
	    player.body.collideWorldBounds = true;
	    // player.animations.add('left', [0, 1, 2, 3], 10, true);
	    // player.animations.add('right', [5, 6, 7, 8], 10, true);
	    player.animations.add('left', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 21, true);
	    player.animations.add('right', [11, 12, 13, 14, 15, 16, 17, 18, 19, 20], 21, true);

	    //Camera
	    this.game.camera.follow(player, Phaser.Camera.FOLLOW_LOCKON);

	    //Score 
	    this.score = 0;
	    this.scoreText = this.game.add.text(7, 0, 'Score: 0', { font: "20px Arial", fill: "#ffffff", align: "center" });
	    this.scoreText.fixedToCamera = true;

	    cursors = this.game.input.keyboard.createCursorKeys();

        //sound manager
        soundSprite = this.game.add.audio('sfx');
        ingamesound = this.game.add.audio('ingamesound');
        walksound = this.game.add.audio('sfx');
        //ingamesound.play();

        soundSprite.addMarker('alien death', 1, 1.0);
        soundSprite.addMarker('boss hit', 3, 0.5);
        soundSprite.addMarker('escape', 4, 3.2);
        soundSprite.addMarker('meow', 8, 0.5);
        soundSprite.addMarker('numkey', 9, 0.1);
        soundSprite.addMarker('ping', 10, 1.0);
        soundSprite.addMarker('death', 12, 4.2);
        soundSprite.addMarker('shot', 17, 1.0);
        walksound.addMarker('squit', 19, 0.3);


	},
	update: function(){
		if(this.score == 120) {
			respawn(this.game, 120);
		}
		this.game.physics.arcade.collide(player, platforms);
		this.game.physics.arcade.collide(crystals, platforms);
	    this.game.physics.arcade.overlap(player, crystals, collectCrystal, null, this);

	    player.body.velocity.x = 0;

	    if(isLeftInputActive(this.game, this.input)) {
	    	player.body.velocity.x = -175;
	    	player.animations.play('left');
	    } else if(isRightInputActive(this.game, this.input)) {
	    	player.body.velocity.x = +175;
	    	player.animations.play('right');
	    } else {
	        player.animations.stop();
	        player.frame = 10;
	    }
	    
	    //if (cursors.up.isDown && player.body.touching.down)
	    if(isJumpInputActive(this.game, this.input) && player.body.touching.down) {
	        player.body.velocity.y = -(350*jumpBoost);
	    }

	    //48 is sprite height, 5 is border bottom height
	    if(player.position.y+(48*scale) == this.world.height) {
	    	respawn(this.game, this.score);
	    }

	    function isLeftInputActive(gameObj, inputObj){
	    	var isActive = false;

		    isActive = inputObj.keyboard.isDown(Phaser.Keyboard.LEFT);
		    isActive |= (gameObj.input.activePointer.isDown && gameObj.input.activePointer.x < gameObj.width/3);

		    return isActive;
	    }
	    function isJumpInputActive(gameObj, inputObj){
	    	var isActive = false;

		    isActive = inputObj.keyboard.isDown(Phaser.Keyboard.UP);
		    isActive |= (gameObj.input.activePointer.isDown && 
		    		gameObj.input.activePointer.x > gameObj.width/3 && gameObj.input.activePointer.x < (gameObj.width/3 * 2));

		    return isActive;
	    }
	    function isRightInputActive(gameObj, inputObj){
	    	var isActive = false;

		    isActive = inputObj.keyboard.isDown(Phaser.Keyboard.RIGHT);
		    isActive |= (gameObj.input.activePointer.isDown && gameObj.input.activePointer.x > (gameObj.width/3 * 2));

		    return isActive;
	    }

	    function collectCrystal(obj1, obj2){
	    	crystals.children[obj2.z-1].kill();
	    	this.score += 10;
	    	this.scoreText.text = 'Score: ' + this.score;
	    }
	    function respawn(obj1, score){
	    	// obj1.reset(35, 20);
	    	// obj2.x = 0;
	    	// obj2.reset();
	    	obj1.state.start("GameOver", true, false, score);
	    }
	}
};